#Support : Vincent LOPEZ - mail vlopez@niit.fr
#!/bin/sh
#Script Server_Side de la vérrifications de la sauvegarde
#
# Exemple :
#		- ./script.sh test
#				Les DATA doivent être dans /home/test/backup/
#				Les logs doivent être dans /home/test/logs/
#			Vous pouvez rajouter jusqu'a 2 destinataire en plus de $MailLogs
#			pour celà : ./script.sh test mail1@domail.tld mail2@domain.tld
Init()
{
	#Déclaration des variables.
	MailLogs="mail@domail.tld"
	check_dir=$(echo /home/$client/logs/)
	temp_logs='temp_logs.txt'
	error_message='/home/presta/script/error_message.txt'
	success_message='/home/presta/script/success_message.txt'
	LogExist
}
GetDate()
{
	datesynchro=$(date '+%Y-%m-%d-%H-%M')
}
LogExist()
{
	#Vérrification de la présence du fichier
	if [ -f $check_dir$temp_logs ];
	then
	Send_Mail_Success
	else
	Send_Mail_Error
	fi
}
Send_Mail_Success()
{
	#Envoi des logs à l'utilisateur
	GetDate
	new_name=$(echo $datesynchro'_DATA.txt')
	#Si $2 Existe alors il enverra 2 Messages.
	if [ -z "$MailSecondaire" ]
	then
		cat $success_message | mail -s "$client | Sauvegarde DATA" $MailLogs
	else
		#Si $3 Existe alors il enverra 3 messages.
		if [ -z "$MailTerciaire" ]
			then
				cat $success_message | mail -s "$client | Sauvegarde DATA" $MailLogs $MailSecondaire
			else
				cat $success_message | mail -s "$client | Sauvegarde DATA" $MailLogs $MailSecondaire $MailTerciaire
			fi
	fi
	mv $check_dir$temp_logs $check_dir/$new_name
}
Send_Mail_Error()
{
	#Nous envoyons un mail d'erreur
	error_construct="_ERREUR_DE_SAUVEGARDE"
	error_title=$(echo $client$error_construct)
	cat $error_message | mail -s $error_title $MailLogs 
}
#Initialisation du script, Récuperation des arguments passé en ligne de commande.
client=$(echo $1)
MailSecondaire=$(echo $2)
MailTerciaire=$(echo $3)
Init
