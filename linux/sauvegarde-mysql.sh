#!/bin/bash
# Backup des bases de données du serveur
# NÉCESSITE AU CHOIX:
# LFTP SUR VOTRE SERVEUR (apt install lftp)
# OU RSYNC AVEC CERTIFICATS DE CONNEXION

# Définitions des variables
# Date du jour au format AnnéeMoisJour pour que les fichiers soient triés automatiquement par date
datejour=$(date +%Y%m%d)
# Emplacement du dossier de travail
localpath=~/backup/BDD
# Connexion de MariaDB/MySQL
MARIAUSER=root
MARIAPASS="Ceci est un putain de mot de passe en or 24 carats serti de diamants!"
email=user@mail.tld
# Variables pour le serveur distant
remoteserver=serveur-backup.domaine.fr
#remoteport=21
remoteport=22
remotepath=/srv/backup/nomduserveur/bdd
username=nom-dutilisateur
password="Phrase 2 passe ultra-secure of the dead!"

# On afficher la date du jour pour le log.
echo "date du jour : $datejour"

# On se déplace dans le dossier de travail pour les sauvegardes
cd $localpath

# on supprime les anciennes archives par souci d'espace disque
rm -f *.tar.gz

# on backup les bases dans des sql differents, la syntaxe est : 
#mysqldump -h _host_ -u _user_ -p_password_ _dbname_ > fichier_sortie.sql
databases=$(mysql --user=$MARIAUSER --password=$MARIAPASS -e "SHOW DATABASES;" | tr -d "| " | grep -v Database)
# on catche les erreurs
if [ ${?} -eq "0" ]; then
  for db in $databases; do
    if [[ "$db" != "information_schema" ]] && [[ "$db" != "performance_schema" ]] && [[ "$db" != "mysql" ]] && [[ "$db" != "phpmyadmin" ]] && [[ "$db" != _* ]] ; then
      echo "Sauvegarde de: $db"
      mysqldump --force --opt --user=$MARIAUSER --password=$MARIAPASS --databases $db > $db.sql
    fi
  done
else
# epic fail, quitte et envoie rapport
echo "--------------- FAIL DETECTED! ABORT! ---------------"
tail /var/log/sauvegarde_sql.log | mail -s "Erreur sauvegarde SQL" $email
exit 1
fi
echo "OK"
# on archive tous les backup a la date du jour 
tar -czf $datejour-nomServeurSQL.tar.gz *.sql

# on supprime les restes des backup dans le dossier d'éxécution 
rm -f *.sql

# le fichier du jour est envoyé sur le serveur 
#lftp -u $username,$password -e "mirror --reverse --verbose $localpath $remotepath" "$remoteserver $remoteport" << bye
rsync -avP -e 'ssh -o StrictHostKeyChecking=no -p $remoteport' $localpath $username@$remoteserver:$remotepath
echo "Termine"
echo "##################################################"
echo .
